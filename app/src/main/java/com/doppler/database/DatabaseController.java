package com.doppler.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by admin on 10/6/2018.
 */

public class DatabaseController extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "NOTIFICATION.db";

    public DatabaseController(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE NOTIFICATIONS(ID INTEGER PRIMARY KEY AUTOINCREMENT, STATUS TEXT);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS NOTIFICATIONS;");
        onCreate(db);
    }

    public void insertNotificationStatus(String status) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("STATUS", status);
        this.getWritableDatabase().insertOrThrow("NOTIFICATIONS", "", contentValues);
        this.getWritableDatabase().close();
    }

    public void updateNotificationStatus(String status) {
        this.getWritableDatabase().execSQL("UPDATE NOTIFICATIONS SET STATUS='" + status + "' WHERE ID=1");
        this.getWritableDatabase().close();
    }

    public String listLastNotification() {
        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT * FROM NOTIFICATIONS ORDER BY ID DESC LIMIT 1;", null);
        String notificationStatus = "NO-STATUS";

        try {
            while (cursor != null && cursor.moveToNext()) {
                notificationStatus = cursor.getString(1);
            }
        } catch (AssertionError e){
            Log.e(DATABASE_NAME, String.format("Failed to get last database notification!", e.getMessage()));
        }finally {
                cursor.close();
                this.getReadableDatabase().close();
        }

        return  notificationStatus;
    }
}
