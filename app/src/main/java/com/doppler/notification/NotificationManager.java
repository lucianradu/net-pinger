package com.doppler.notification;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;

public class NotificationManager {

    public static void sendNotification(Context context, NotificationType notificationType, int notificationChannel) {


        IntentFilter intentFilter = new IntentFilter();

        Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(""));

        myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, myIntent, 0);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        Uri ringtoneUri = Uri.parse(preferences.getString("notifications_new_message_ringtone", "DEFAULT_RINGTONE_URI"));
        boolean isNotificationSticky = !Boolean.parseBoolean(preferences.getAll().get("stickyNotification").toString());

        Notification.Builder builder;
        builder = new Notification.Builder(context)
                .setContentTitle(notificationType.getNotificationContent())
                .setSmallIcon(notificationType.getNotificationIcon())
                .setOnlyAlertOnce(true)
                .setOngoing(isNotificationSticky)
                .setSubText(notificationType.getNotificationTitle())
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setSound(ringtoneUri)
                .setColor(notificationType.getNotificationColor())
                .setContentIntent(pendingIntent);


        Notification notification = builder.build();
        android.app.NotificationManager notificationManager = (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        boolean notificationEnabled = true;

        if (preferences.getAll().get("notification_switch") != null) {
             notificationEnabled = Boolean.parseBoolean(preferences.getAll().get("notification_switch").toString());
        }

        if (notificationEnabled) {
            notificationManager.notify(notificationChannel, notification);

            //dismiss old notification
            if (notificationType.equals(NotificationType.INTERNET_UP)) {
                notificationManager.cancel(3);
            } else {
                notificationManager.cancel(1);
            }

        } else {
            notificationManager.cancel(notificationChannel);
        }

        Intent localIntent = new Intent("internet.status.broadcast");
        localIntent.putExtra("internetStatus", notificationType);
        LocalBroadcastManager.getInstance(context).sendBroadcast(localIntent);

    }

}