package com.doppler.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.doppler.network.NetworkStatus;

import static android.content.ContentValues.TAG;

public class NetworkChangeReceiver extends BroadcastReceiver {

    private static final String TAG = "NETWORK-STATUS";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onBroadcast: Network changed");
        NetworkStatus networkStats = new NetworkStatus();

        Intent local = new Intent();
        local.setAction("service.to.activity.transfer");

        if (networkStats.getNetworkConnectionType(context).equals("wifi-connection")) {
            Log.d(TAG, "onBroadcast: Network changed: wifi connection");
            local.putExtra("connectionType", "wifi-connection");
            context.sendBroadcast(local);
        } else if (networkStats.getNetworkConnectionType(context).equals("data-connection")) {
            Log.d(TAG, "onBroadcast: Network changed: data connection");
            local.putExtra("connectionType", "data-connection");
            context.sendBroadcast(local);
        } else if (networkStats.getNetworkConnectionType(context).equals("vpn-connection")) {
            Log.d(TAG, "onBroadcast: Network changed: vpn connection");
            local.putExtra("connectionType", "vpn-connection");
            context.sendBroadcast(local);
        } else {
            Log.d(TAG, "onBroadcast: Network changed: no connection");
            local.putExtra("connectionType", "no-connection");
            context.sendBroadcast(local);
        }

    }

}