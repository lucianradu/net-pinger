package com.doppler.broadcast;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION_CODES;
import android.os.PowerManager;
import android.util.Log;

public class DeviceIdleReceiver extends BroadcastReceiver {

    public static final String TAG = "PowerManager";

    @TargetApi(VERSION_CODES.M)
    @Override
    public void onReceive(Context context, Intent intent) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        Log.d(TAG, "isDeviceIdle:" + pm.isDeviceIdleMode());
    }
}
